from inova_core.models import ProfesionalBase
import datetime

class MassAutoSync:

    def __init__(self):
        self.get_user_list()

    def get_user_list(self) -> None:
        print("ejecuta la funcion d")
        current_time = datetime.datetime.now().time()
        profesionals = ProfesionalBase.objects.filter(sincronizacion=True, hora_sincronizacion__contains=current_time.hour())
        if profesionals:
            for profesional in profesionals:
                self.send_push_notification(profesional.fcm_token)
                return "success"
        return "not professional for this hour"
    
    def send_push_notification(self, device_token):
        server_token = settings.SERVER_NOTIFICATION_TOKEN
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'key=' + server_token
        }
        body = {
            "to": device_token,
            "priority": "high",
            "data": {
                "autosync": "true"
            },
        }
        response = requests.post("https://fcm.googleapis.com/fcm/send", headers=headers, data=json.dumps(body))
        return response



